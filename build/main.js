"use strict";

var name1 = 'Regina';
var url = "images/".concat(name.toLowerCase(), ".jpg"); //let html = ``;

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}]; //const {name,base,price_small,price_large,image} = data;

/*
    function compareByName(a,b){
        if(a.name > b.name){
            return 1;
        }
        if(a.name < b.name){
            return -1;
        }
        return 0;
    }
    */

function compareBySmallPrice(a, b) {
  if (a.price_small === b.price_small) {
    return a.price_large - b.price_large;
  }

  return a.price_small - b.price_small;
}

data.sort(compareBySmallPrice);

var filterBase = function filterBase(_ref) {
  var base = _ref.base;
  return base == 'tomate';
};

var filterPrice = function filterPrice(_ref2) {
  var price_small = _ref2.price_small;
  return price_small < 6;
};

function filterByDoublI(_ref3) {
  var name = _ref3.name;
  var res = name.split('i');
  return res.length == 3;
} // const res = data.filter(filterPrice);


var reducer = function reducer(html, _ref4) {
  var name = _ref4.name,
      price_large = _ref4.price_large,
      price_small = _ref4.price_small,
      image = _ref4.image;
  return html + "<article class=\"pizzaThumbnail\">\n         <a href=\"".concat(image, "\">\n             <img src=\"").concat(image, "\"/>\n             <section> \n                 <h4>").concat(name, "</h4>\n                 <ul>\n                     <li> Prix petit format : ").concat(price_small.toFixed(2), " \u20AC</li>\n                     <li> Prix grand format : ").concat(price_large.toFixed(2), " \u20AC</li>\n                 </ul>\n             </section>\n         </a>\n     </article>");
};

console.log(data.reduce(reducer)); // res.forEach(({name,price_large,price_small,image}) =>
//     html = html +
//     `<article class="pizzaThumbnail">
//         <a href="${image}">
//             <img src="${image}"/>
//             <section> 
//                 <h4>${name}</h4>
//                 <ul>
//                     <li> Prix petit format : ${price_small.toFixed(2)} €</li>
//                     <li> Prix grand format : ${price_large.toFixed(2)} €</li>
//                 </ul>
//             </section>
//         </a>
//     </article>`
//     );

document.querySelector('.pageContent').innerHTML = data.reduce(reducer, "");
//# sourceMappingURL=main.js.map